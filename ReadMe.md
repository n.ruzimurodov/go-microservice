# GO test Project

This is a test project for GO and consists of three microservices:

-
    1. first microservice just collect posts from given endpoint and save them to database
-
    2. second microservice has CRUD operations for posts
-
    3. third microservice is API gateaway for first two microservices

## Installation

## running service 1 (collecting posts)

```bash
cd service1
go run main.go
```

## running service 2 (CRUD operations)

```bash
cd service2
go run main.go
```

## running service 3 (API gateaway)

```bash
cd service3
go run main.go
```