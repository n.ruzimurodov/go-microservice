package main

import (
	"github.com/rs/cors"
	"google.golang.org/grpc"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	httpSwagger "github.com/swaggo/http-swagger"
	_ "service3/docs"
	"service3/handler"
	pr1 "service3/protos/service1/proto"
	pr2 "service3/protos/service2/proto"
)

func main() {
	conn1, err := grpc.Dial("localhost:50050", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Failed to connect to service1 gRPC server: %v", err)
	}
	defer conn1.Close()

	conn2, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Failed to connect to service2 gRPC server: %v", err)
	}
	defer conn2.Close()

	// Initialize the gRPC client connections
	collectorClient := pr1.NewCollectorClient(conn1)
	service2Client := pr2.NewService2Client(conn2)

	// Create a new handler with the gRPC clients
	handler := handler.NewHandler(collectorClient, service2Client)

	router := mux.NewRouter()
	corsHandler := cors.Default().Handler(router)
	router.HandleFunc("/collect", handler.CollectPosts).Methods("POST")
	router.HandleFunc("/posts", handler.GetPosts).Methods("GET")
	router.HandleFunc("/posts/{id}", handler.GetPost).Methods("GET")
	router.HandleFunc("/posts/{id}", handler.DeletePost).Methods("DELETE")
	router.HandleFunc("/posts/{id}", handler.UpdatePost).Methods("PUT")
	http.HandleFunc("/docs/", httpSwagger.WrapHandler)
	router.PathPrefix("/swagger/").Handler(httpSwagger.Handler(
		httpSwagger.URL("http://localhost:8080/swagger/doc.json"), // Replace with your API server address
	))

	log.Println("API Gateway server started on port 8080")
	http.ListenAndServe(":8080", corsHandler)
}
