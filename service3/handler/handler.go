package handler

import (
	"context"
	"encoding/json"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"strconv"

	pr1 "service3/protos/service1/proto"
	pr2 "service3/protos/service2/proto"
)

type Handler struct {
	collectorClient pr1.CollectorClient
	service2Client  pr2.Service2Client
}

func NewHandler(collectorClient pr1.CollectorClient, service2Client pr2.Service2Client) *Handler {
	return &Handler{
		collectorClient: collectorClient,
		service2Client:  service2Client,
	}
}

func (h *Handler) CollectPosts(w http.ResponseWriter, r *http.Request) {
	// Call the CollectPosts gRPC method of the Collector service
	collectRequest := &pr1.CollectPostsRequest{}
	collectResponse, err := h.collectorClient.CollectPosts(context.Background(), collectRequest)
	if err != nil {
		log.Printf("Failed to collect posts: %v", err)
		http.Error(w, "Failed to collect posts", http.StatusInternalServerError)
		return
	}

	if !collectResponse.Success {
		log.Println("Failed to collect posts")
		http.Error(w, "Failed to collect posts", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Posts collected successfully"))
}

// swagger:route GET /posts GetPosts
//
// Get multiple posts.
//
// This endpoint retrieves multiple posts.
//
// Responses:
//   200: GetPostsResponse
//   404: NotFoundResponse

func (h *Handler) GetPosts(w http.ResponseWriter, r *http.Request) {
	// Call the GetPosts gRPC method of the Service2
	getPostsRequest := &pr2.GetPostsRequest{}
	getPostsResponse, err := h.service2Client.GetPosts(context.Background(), getPostsRequest)
	if err != nil {
		log.Printf("Failed to get posts: %v", err)
		http.Error(w, "Failed to get posts", http.StatusInternalServerError)
		return
	}

	response, err := json.Marshal(getPostsResponse.Posts)
	if err != nil {
		log.Printf("Failed to marshal response: %v", err)
		http.Error(w, "Failed to marshal response", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(response)
}

func (h *Handler) GetPost(w http.ResponseWriter, r *http.Request) {
	postID := mux.Vars(r)["id"]

	// Call the GetPost gRPC method of the Service2
	post_id, err := strconv.ParseInt(postID, 10, 64)
	if err != nil {
		log.Printf("Failed to parse post id: %v", err)
		http.Error(w, "Failed to parse post id", http.StatusInternalServerError)
		return
	}
	getPostRequest := &pr2.GetPostRequest{
		Id: post_id,
	}
	getPostResponse, err := h.service2Client.GetPost(context.Background(), getPostRequest)
	if err != nil {
		log.Printf("Failed to get post: %v", err)
		http.Error(w, "Failed to get post", http.StatusInternalServerError)
		return
	}

	response, err := json.Marshal(getPostResponse.Post)
	if err != nil {
		log.Printf("Failed to marshal response: %v", err)
		http.Error(w, "Failed to marshal response", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(response)
}

func (h *Handler) DeletePost(w http.ResponseWriter, r *http.Request) {
	postID := mux.Vars(r)["id"]

	post_id, err := strconv.ParseInt(postID, 10, 64)
	if err != nil {
		log.Printf("Failed to parse post id: %v", err)
		http.Error(w, "Failed to parse post id", http.StatusInternalServerError)
		return
	}
	// Call the DeletePost gRPC method of the Service2
	deletePostRequest := &pr2.DeletePostRequest{
		Id: post_id,
	}

	res, err := h.service2Client.DeletePost(context.Background(), deletePostRequest)
	println(res)
	if err != nil {
		log.Printf("Failed to delete post: %v", err)
		http.Error(w, "Failed to delete post", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Post deleted successfully"))
}

func (h *Handler) UpdatePost(w http.ResponseWriter, r *http.Request) {
	postID := mux.Vars(r)["id"]

	// Parse the request body into a Post struct
	var post pr2.Post
	err := json.NewDecoder(r.Body).Decode(&post)
	if err != nil {
		log.Printf("Invalid request body: %v", err)
		http.Error(w, "Invalid request body", http.StatusBadRequest)
		return
	}
	post_id, err := strconv.ParseInt(postID, 10, 64)
	if err != nil {
		log.Printf("Failed to parse post id: %v", err)
		http.Error(w, "Failed to parse post id", http.StatusInternalServerError)
		return
	}

	// Call the UpdatePost gRPC method of the Service2
	updatePostRequest := &pr2.UpdatePostRequest{
		Id:   post_id,
		Post: &post,
	}
	_, err = h.service2Client.UpdatePost(context.Background(), updatePostRequest)
	if err != nil {
		log.Printf("Failed to update post: %v", err)
		http.Error(w, "Failed to update post", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Post updated successfully"))
}
