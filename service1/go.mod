module service1

go 1.16

require (
	github.com/joho/godotenv v1.4.0
	github.com/lib/pq v1.10.9
	google.golang.org/grpc v1.56.2
	google.golang.org/protobuf v1.30.0
)
