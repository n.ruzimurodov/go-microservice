package database

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"log"
	"service2/internal/models"
)

type DB struct {
	*sql.DB
}

func Connect() (*DB, error) {
	connStr := "postgresql://postgres:postgres@localhost:5432/microservice?sslmode=disable"
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return nil, err
	}

	err = db.Ping()
	if err != nil {
		return nil, err
	}

	log.Println("Connected to the database")
	return &DB{db}, nil
}
func (db *DB) GetPosts() ([]models.Post, error) {
	query := "SELECT * FROM posts"

	rows, err := db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var posts []models.Post
	for rows.Next() {
		var post models.Post
		err := rows.Scan(&post.ID, &post.Title, &post.Body, &post.UserID)
		if err != nil {
			return nil, err
		}
		posts = append(posts, post)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return posts, nil
}

func (db *DB) GetPost(id string) (*models.Post, error) {
	query := "SELECT * FROM posts WHERE id = $1"

	row := db.QueryRow(query, id)

	var post models.Post
	err := row.Scan(&post.ID, &post.Title, &post.Body, &post.UserID)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, fmt.Errorf("post not found")
		}
		return nil, err
	}

	return &post, nil
}

func (db *DB) DeletePost(id string) error {
	query := "DELETE FROM posts WHERE id = $1"

	result, err := db.Exec(query, id)
	if err != nil {
		return err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return err
	}

	if rowsAffected == 0 {
		return fmt.Errorf("post not found")
	}

	return nil
}

func (db *DB) UpdatePost(id string, post *models.Post) error {
	query := "UPDATE posts SET title = $1, body = $2 WHERE id = $3"

	result, err := db.Exec(query, post.Title, post.Body, id)
	if err != nil {
		return err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return err
	}

	if rowsAffected == 0 {
		return fmt.Errorf("post not found")
	}

	return nil
}
