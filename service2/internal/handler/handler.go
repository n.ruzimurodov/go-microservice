package handler

import (
	"context"
	"fmt"
	"service2/internal/models"
	"strconv"

	"service2/internal/database"
	pb "service2/service2/proto"
)

type Handler struct {
	db *database.DB
	pb.UnimplementedService2Server
}

func NewHandler() *Handler {
	// Create a new database connection
	db, _ := database.Connect()

	return &Handler{
		db: db,
	}
}

func (h *Handler) GetPosts(ctx context.Context, req *pb.GetPostsRequest) (*pb.GetPostsResponse, error) {
	// Retrieve posts from the database
	posts, err := h.db.GetPosts()
	if err != nil {
		return nil, fmt.Errorf("failed to retrieve posts: %v", err)
	}

	// Convert the posts to the gRPC response format
	var pbPosts []*pb.Post
	for _, post := range posts {
		pbPost := &pb.Post{
			Id:     post.ID,
			Title:  post.Title,
			Body:   post.Body,
			UserId: post.UserID,
		}
		pbPosts = append(pbPosts, pbPost)
	}

	response := &pb.GetPostsResponse{
		Posts: pbPosts,
	}

	return response, nil
}

func (h *Handler) GetPost(ctx context.Context, req *pb.GetPostRequest) (*pb.GetPostResponse, error) {
	// Retrieve the post from the database
	id := strconv.FormatInt(req.Id, 10)
	post, err := h.db.GetPost(id)
	if err != nil {
		return nil, fmt.Errorf("failed to retrieve post: %v", err)
	}

	// Convert the post to the gRPC response format
	pbPost := &pb.Post{
		Id:     post.ID,
		Title:  post.Title,
		Body:   post.Body,
		UserId: post.UserID,
	}

	response := &pb.GetPostResponse{
		Post: pbPost,
	}

	return response, nil
}

func (h *Handler) DeletePost(ctx context.Context, req *pb.DeletePostRequest) (*pb.DeletePostResponse, error) {
	// Delete the post from the database
	id := strconv.FormatInt(req.Id, 10)
	err := h.db.DeletePost(id)
	if err != nil {
		return nil, fmt.Errorf("failed to delete post: %v", err)
	}

	// Create an empty response
	response := &pb.DeletePostResponse{}

	return response, nil
}

func (h *Handler) UpdatePost(ctx context.Context, req *pb.UpdatePostRequest) (*pb.UpdatePostResponse, error) {
	// Convert the gRPC post to the internal model
	post := &models.Post{
		ID:     req.Id,
		Title:  req.Post.Title,
		Body:   req.Post.Body,
		UserID: req.Post.UserId,
	}
	id := strconv.FormatInt(req.Id, 10)

	// Update the post in the database
	err := h.db.UpdatePost(id, post)
	if err != nil {
		return nil, fmt.Errorf("failed to update post: %v", err)
	}

	response := &pb.UpdatePostResponse{}

	return response, nil
}

func (h *Handler) mustEmbedUnimplementedService2Server() {}
