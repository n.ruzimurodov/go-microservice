package main

import (
	"log"
	"net"

	"google.golang.org/grpc"
	"service2/internal/database"
	"service2/internal/handler"
	pb "service2/service2/proto"
)

func main() {
	db, err := database.Connect()
	if err != nil {
		log.Fatal("Failed to connect to the database:", err)
	}
	println(db)

	h := handler.NewHandler()

	lis, err := net.Listen("tcp", ":50051")
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	s := grpc.NewServer()
	pb.RegisterService2Server(s, h)

	log.Println("Server started on port 50051")
	if err := s.Serve(lis); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}
}
